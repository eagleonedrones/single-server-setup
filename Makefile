.PHONY: build run

build:
	docker-compose build \
	 --build-arg GIT_PRIVATE_KEY="$(shell cat ~/.ssh/id_rsa | perl -pe 's/\n/__x__/g')"

run:
	docker-compose up

clean:
	docker-compose down --remove-orphans -v
